<?php
if (empty($_GET['id'])) {
    header('Location: /index.php');
    die();
}

session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once '../classes/HtmlMessageWriter.php';

require_once '../classes/Professor.php';
require_once '../classes/HtmlProfessorWriter.php';

$professor = new Professor();
$html = HtmlProfessorWriter::writeUpdateForm($professor->getById($_GET['id']));


/*
 * html output
 */
require_once '../parts/header.php';
echo HtmlMessageWriter::writeMessage($message);
echo $html;
require_once '../parts/footer.php';