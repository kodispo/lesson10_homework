<?php
if (empty($_GET['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Professor.php';
require_once '../classes/HtmlProfessorWriter.php';

$professor = new Professor();
$html = HtmlProfessorWriter::writeDetails($professor->getById($_GET['id']));


/*
 * html output
 */
require_once '../parts/header.php';
echo $html;
require_once '../parts/footer.php';