<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

if (empty($_POST['first_name']) ||
    empty($_POST['last_name']) ||
    empty($_POST['email']) ||
    empty($_POST['department_id']) ||
    empty($_POST['subjects'])
) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to update professor information. Required fields must be specified'
    ];
    header('Location: /professor/edit.php?id=' . $_POST['id']);
    die();
}

require_once '../classes/Professor.php';

$professor = new Professor($_POST['first_name'], $_POST['last_name'], $_POST['email'], $_POST['department_id']);
$professor->setId($_POST['id']);
$professor->update();
$professor->setSubjects($_POST['subjects']);

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Professor information has been updated successfully'
];
header('Location: /professor/edit.php?id=' . $_POST['id']);