<?php
require_once 'Core.php';
require_once 'Department.php';
require_once 'Subject.php';

class Professor extends Core {
    
    static protected $table = 'professors';
    protected $id;
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $department_id;
    
    public function __construct($first_name = '', $last_name = '', $email = '', $department_id = null) {
        parent::__construct();
        $this->first_name = htmlspecialchars($first_name);
        $this->last_name = htmlspecialchars($last_name);
        $this->email = htmlspecialchars($email);
        $this->department_id = (int) htmlspecialchars($department_id);
    }
    
    public function getAll() {
        try {
        
            $sql = 'SELECT id, first_name, last_name, email, department_id FROM ' . self::$table;
            $stmt = $this->pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Professor');
            return $stmt->fetchAll();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve all professors!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function create() {
        try {
        
            $sql = 'INSERT INTO ' . self::$table . ' SET
            first_name = :first_name,
            last_name = :last_name,
            email = :email,
            department_id = :department_id';
        
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':first_name' => $this->first_name,
                ':last_name' => $this->last_name,
                ':email' => $this->email,
                ':department_id' => $this->department_id,
            ]);
            $this->id = $this->pdo->lastInsertId();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, first_name, last_name, email, department_id
                FROM ' . self::$table . '
                WHERE id = :id
            ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Professor');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve professor information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        try {
        
            $sql = 'UPDATE ' . self::$table . ' SET
            first_name = :first_name,
            last_name = :last_name,
            email = :email,
            department_id = :department_id
            WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':first_name' => $this->first_name,
                ':last_name' => $this->last_name,
                ':email' => $this->email,
                ':department_id' => $this->department_id,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        try {
        
            $sql = 'DELETE FROM ' . self::$table . ' WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to remove professor!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getDepartment() {
        $departmentObject = new Department();
        return $departmentObject->getById($this->department_id);
    }
    
    public function getSubjects() {
        try {
            
            $sql = 'SELECT subject_id FROM professor_subject WHERE professor_id = ' . $this->id;
            $stmt = $this->pdo->query($sql);
            $subjectIds = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $subjects = [];
            if (!empty($subjectIds)) {
                foreach ($subjectIds as $subjectId) {
                    $subjectObject = new Subject();
                    $subjects[] = $subjectObject->getById($subjectId['subject_id']);
                }
            }
            
            return $subjects;
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve professors\'s subjects!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function setSubjects($subjects) {
        try {
            
            $sql = 'DELETE FROM professor_subject WHERE professor_id = ' . $this->id;
            $this->pdo->exec($sql);
            
            $sql = 'INSERT INTO professor_subject SET
            professor_id = :professor_id,
            subject_id = :subject_id';
            $stmt = $this->pdo->prepare($sql);
            
            if (is_array($subjects) && !empty($subjects)) {
                foreach ($subjects as $subject) {
                    $stmt->execute([
                        ':professor_id' => $this->id,
                        ':subject_id' => $subject,
                    ]);
                }
            }
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to add subjects!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getFirstName() {
        return $this->first_name;
    }
    
    public function getLastName() {
        return $this->last_name;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getDepartmentId() {
        return $this->department_id;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}