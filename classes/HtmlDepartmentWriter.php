<?php
class HtmlDepartmentWriter {
    
    static public function writeTable($departments) {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Departments</h1>
        <div class="table-responsive mb-4">
            <table class="table mb-0">
                <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Phone</th>
                <th></th>
                </thead>
                <tbody>
                <?php if (!empty($departments)) : ?>
                    <?php foreach ($departments as $department) : ?>
                        <tr>
                            <td><?= $department->getId(); ?></td>
                            <td><?= $department->getName(); ?></td>
                            <td><?= $department->getPhone(); ?></td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <a href="/department/details.php?id=<?= $department->getId(); ?>" class="btn btn-outline-primary btn-sm">Details</a>
                                    <a href="/department/edit.php?id=<?= $department->getId(); ?>" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                    <form action="/department/delete.php" method="post">
                                        <input type="hidden" name="id" value="<?= $department->getId(); ?>">
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6">
                            <span class="text-danger">Empty</span>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="text-right">
                        <a href="/department/new.php" class="btn btn-outline-primary btn-sm">Add department</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeCreateForm() {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Add new department</h1>
        <form action="/department/create.php" method="post">
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label">Name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="name" name="name" placeholder="Enter name" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-md-2 col-form-label">Phone<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="phone" name="phone" placeholder="Enter phone" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Add department</button>
                </div>
            </div>
        </form>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeUpdateForm(Department $department) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase">Update department</h1>
        <form action="/department/update.php" method="post">
            <input type="hidden" name="id" value="<?= $department->getId(); ?>">
            <div class="form-group row">
                <label for="name" class="col-md-2 col-form-label">Name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="name" name="name" value="<?= $department->getName(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-md-2 col-form-label">Phone</label>
                <div class="col-md-10">
                    <input type="text" id="phone" name="phone" value="<?= $department->getPhone(); ?>" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Update department</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeDetails(Department $department) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase"><?= $department->getName(); ?></h1>
        <h2 class="h6 mb-4"><?= $department->getPhone(); ?></h2>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
}