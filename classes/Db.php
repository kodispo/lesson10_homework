<?php
require_once 'Core.php';

class Db extends Core {
    
    public function createTables() {
        try {
            
            $sql = "CREATE TABLE departments (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR (255) NOT NULL,
                phone VARCHAR (255) NOT NULL
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $this->pdo->exec($sql);

            $sql = "CREATE TABLE professors (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                first_name VARCHAR (255) NOT NULL,
                last_name VARCHAR (255) NOT NULL,
                email VARCHAR (255) NOT NULL,
                department_id INT,
                FOREIGN KEY (department_id) REFERENCES departments (id) ON DELETE SET NULL
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $this->pdo->exec($sql);
    
            $sql = "CREATE TABLE subjects (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR (255) NOT NULL
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $this->pdo->exec($sql);
    
            $sql = "CREATE TABLE professor_subject (
                professor_id INT NOT NULL,
                subject_id INT NOT NULL,
                FOREIGN KEY (professor_id) REFERENCES professors (id) ON DELETE CASCADE,
                FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
            $this->pdo->exec($sql);

        } catch (Exception $exception) {

            echo '<strong>Failed to create tables!</strong><br>' . $exception->getMessage();
            die();

        }
    }
    
    public function populateTables() {
        try {
            
            $sql = '
                INSERT INTO departments (name, phone)
                VALUES
                    ("Aeronautics & Astronautics Department", "+1-202-555-0150"),
                    ("Physics Department", "+1-202-555-0182"),
                    ("Mathematics Department", "+1-202-555-0179")
            ';
            $this->pdo->exec($sql);
            
            $sql = '
                INSERT INTO professors (first_name, last_name, email, department_id)
                VALUES
                    ("Karla", "Salter", "salter@gmail.com", 1),
                    ("Casper", "Watkins", "watkins@gmail.com", 1),
                    ("Poppie", "Zuniga", "zuniga@gmail.com", 2),
                    ("Antony", "Appleton", "appleton@gmail.com", 2),
                    ("Hawwa", "Cortez", "cortez@gmail.com", 3),
                    ("Amrit", "Lara", "lara@gmail.com", 3)
            ';
            $this->pdo->exec($sql);
            
            $sql = '
                INSERT INTO subjects (name)
                VALUES
                    ("Astrodynamics"),
                    ("Computational Fluid Dynamics"),
                    ("Space Plasmas"),
                    ("Physics"),
                    ("Quantum Physics"),
                    ("Electromagnetism"),
                    ("Algebra"),
                    ("Geometry"),
                    ("Statistics")
            ';
            $this->pdo->exec($sql);
            
            $sql = '
                INSERT INTO professor_subject (professor_id, subject_id)
                VALUES
                    (1, 1), (1, 2), (1, 3),
                    (2, 2), (2, 3),
                    (3, 4), (3, 5), (3, 6),
                    (4, 4), (4, 6),
                    (5, 7), (5, 8),
                    (6, 8), (6, 9)
            ';
            $this->pdo->exec($sql);
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to populate tables!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function removeTables() {
        try {
    
            $this->pdo->exec("DROP TABLE professor_subject, subjects, professors, departments");
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to remove tables!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
}