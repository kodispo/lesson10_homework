<?php
class HtmlMessageWriter {
    
    static public function writeMessage($message = null) {
        $html = '';
        if (!empty($message)) :
            $type = ($message['type'] == 'success') ? 'alert-success' : 'alert-danger';
            ob_start(); ?>
            
            <div class="alert alert-dismissible fade show mb-5 <?= $type; ?>" role="alert">
                <?= $message['text']; ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <?php $html = ob_get_contents();
            ob_end_clean();
        endif;
        return $html;
    }
    
}