<?php
require_once 'Core.php';

class Subject extends Core {
    
    static protected $table = 'subjects';
    protected $id;
    protected $name;
    
    public function __construct($name = '') {
        parent::__construct();
        $this->name = htmlspecialchars($name);
    }
    
    public function getAll() {
        try {
        
            $sql = 'SELECT id, name FROM ' . self::$table;
            $stmt = $this->pdo->query($sql);
            $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Subject');
            return $stmt->fetchAll();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to retrieve all subjects!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function create() {
        try {
        
            $sql = 'INSERT INTO ' . self::$table . ' SET name = :name';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':name' => $this->name,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to add new subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function getById($id) {
        $id = (int) htmlspecialchars($id);
        
        try {
            
            $sql = '
                SELECT id, name
                FROM ' . self::$table . '
                WHERE id = :id
            ';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Subject');
            return $stmt->fetch();
            
        } catch (Exception $exception) {
            
            echo '<strong>Failed to retrieve subject information!</strong><br>' . $exception->getMessage();
            die();
            
        }
    }
    
    public function update() {
        try {
        
            $sql = 'UPDATE ' . self::$table . ' SET
            name = :name
            WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute([
                ':id' => $this->id,
                ':name' => $this->name,
            ]);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to update subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function delete() {
        try {
        
            $sql = 'DELETE FROM ' . self::$table . ' WHERE id = :id';
            $stmt = $this->pdo->prepare($sql);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to remove subject!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function setId($id) {
        $this->id = (int) htmlspecialchars($id);
    }
    
}