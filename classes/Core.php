<?php
class Core {
    
    static protected $table = '';
    protected $pdo;
    
    public function __construct() {
        $host = 'localhost';
        $dbuser = 'root';
        $password = 'root';
        $dbname = 'lesson10_homework';
    
        try {
    
            $pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $dbuser, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMES "utf8"');
            $this->pdo = $pdo;
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to connect to database!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
    public function hasTable() {
        try {
        
            $sql = 'SHOW TABLES LIKE "' . self::$table . '"';
            $stmt = $this->pdo->query($sql);
            return $stmt->fetch(PDO::FETCH_ASSOC);
        
        } catch (Exception $exception) {
        
            echo '<strong>Failed to define if table "' . self::$table . '" exists!</strong><br>' . $exception->getMessage();
            die();
        
        }
    }
    
}