<?php
require_once 'Department.php';
require_once 'Subject.php';

class HtmlProfessorWriter {
    
    static public function writeTable($professors) {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Professors</h1>
        <div class="table-responsive mb-4">
            <table class="table mb-0">
                <thead>
                <th>ID</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Department</th>
                <th></th>
                </thead>
                <tbody>
                <?php if (!empty($professors)) : ?>
                    <?php foreach ($professors as $professor) : ?>
                        <tr>
                            <td><?= $professor->getId(); ?></td>
                            <td><?= $professor->getFirstName(); ?></td>
                            <td><?= $professor->getLastName(); ?></td>
                            <td><?= $professor->getEmail(); ?></td>
                            <td>
                                <?php
                                if ($department = $professor->getDepartment()) {
                                    echo $department->getName();
                                } else {
                                    echo 'not specified';
                                }
                                ?>
                            </td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <a href="/professor/details.php?id=<?= $professor->getId(); ?>" class="btn btn-outline-primary btn-sm">Details</a>
                                    <a href="/professor/edit.php?id=<?= $professor->getId(); ?>" class="btn btn-outline-primary btn-sm mx-1">Edit</a>
                                    <form action="/professor/delete.php" method="post">
                                        <input type="hidden" name="id" value="<?= $professor->getId(); ?>">
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Remove</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6">
                            <span class="text-danger">Empty</span>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="text-right">
                        <a href="/professor/new.php" class="btn btn-outline-primary btn-sm">Add professor</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeCreateForm() {
        $html = '';
        ob_start(); ?>
    
        <h1 class="h4 mb-3 text-uppercase">Add new professor</h1>
        <form action="/professor/create.php" method="post">
            <div class="form-group row">
                <label for="first_name" class="col-md-2 col-form-label">First name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="first_name" name="first_name" placeholder="Enter first name" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="last_name" class="col-md-2 col-form-label">Last name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="last_name" name="last_name" placeholder="Enter last name" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-2 col-form-label">Email<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="email" name="email" placeholder="Enter email" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="department_id" class="col-md-2 col-form-label">Department<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <?php
                    $departmentObject = new Department();
                    $departments = $departmentObject->getAll();
                    ?>
                    <select class="custom-select form-control" id="department_id" name="department_id">
                        <option value="" selected>Choose department</option>
                        <?php foreach ($departments as $department) : ?>
                            <option value="<?= $department->getId(); ?>"><?= $department->getName(); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="subjects" class="col-md-2 col-form-label">Subjects<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <?php
                    $subjectObject = new Subject();
                    $subjects = $subjectObject->getAll();
                    ?>
                    <select class="custom-select form-control" id="subjects" name="subjects[]" multiple size="9">
                        <?php foreach ($subjects as $subject) : ?>
                            <option value="<?= $subject->getId(); ?>"><?= $subject->getName(); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Add professor</button>
                </div>
            </div>
        </form>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeUpdateForm(Professor $professor) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase">Update professor</h1>
        <form action="/professor/update.php" method="post">
            <input type="hidden" name="id" value="<?= $professor->getId(); ?>">
            <div class="form-group row">
                <label for="first_name" class="col-md-2 col-form-label">First name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="first_name" name="first_name" value="<?= $professor->getFirstName(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="last_name" class="col-md-2 col-form-label">Last name<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="last_name" name="last_name" value="<?= $professor->getLastName(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-2 col-form-label">Email<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <input type="text" id="email" name="email" value="<?= $professor->getEmail(); ?>" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="department_id" class="col-md-2 col-form-label">Department<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <?php
                    $departmentObject = new Department();
                    $departments = $departmentObject->getAll();
                    ?>
                    <select class="custom-select form-control" id="department_id" name="department_id">
                        <option value="" <?php if (!$professor->getDepartmentId()) : ?>selected<?php endif; ?>>Choose department</option>
                        <?php foreach ($departments as $department) : ?>
                            <option value="<?= $department->getId(); ?>"
                                    <?php if ($department->getId() == $professor->getDepartmentId()) : ?>selected<?php endif; ?>
                            >
                                <?= $department->getName(); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="subjects" class="col-md-2 col-form-label">Subjects<span class="text-danger">*</span></label>
                <div class="col-md-10">
                    <?php
                    $subjectObject = new Subject();
                    $subjects = $subjectObject->getAll();
                    ?>
                    <select class="custom-select form-control" id="subjects" name="subjects[]" multiple size="9">
                        <?php foreach ($subjects as $subject) : ?>
                            <?php
                            $selected = '';
                            foreach ($professor->getSubjects() as $selectedSubject) {
                                if ($subject->getId() == $selectedSubject->getId()) {
                                    $selected = 'selected';
                                    break;
                                }
                            }
                            ?>
                            <option value="<?= $subject->getId(); ?>" <?= $selected; ?>>
                                <?= $subject->getName(); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-10">
                    <button type="submit" class="btn btn-outline-primary">Update professor</button>
                </div>
            </div>
        </form>
        
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
    static public function writeDetails(Professor $professor) {
        $html = '';
        ob_start(); ?>

        <h1 class="h4 mb-3 text-uppercase"><?= $professor->getFirstName() . ' ' . $professor->getLastName(); ?></h1>
        <ul class="list-group">
            <li class="list-group-item">
                <strong>Email:</strong> <?= $professor->getEmail(); ?>
            </li>
            <li class="list-group-item">
                <strong>Department:</strong>
                <?php
                if ($department = $professor->getDepartment()) {
                    echo $department->getName();
                } else {
                    echo 'not specified';
                }
                ?>
            </li>
            <li class="list-group-item">
                <?php
                $subjects = [];
                foreach ($professor->getSubjects() as $subject) {
                    $subjects[] = $subject->getName();
                }
                ?>
                <strong>Subjects:</strong> <?= implode(', ', $subjects); ?>
            </li>
        </ul>
    
        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
    
}