<?php
session_start();
require_once '../classes/Db.php';

$db = new Db();
$db->removeTables();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Tables have been removed successfully'
];
header('Location: /index.php');