<?php
if (empty($_GET['id'])) {
    header('Location: /index.php');
    die();
}

require_once '../classes/Department.php';
require_once '../classes/HtmlDepartmentWriter.php';

$department = new Department();
$html = HtmlDepartmentWriter::writeDetails($department->getById($_GET['id']));


/*
 * html output
 */
require_once '../parts/header.php';
echo $html;
require_once '../parts/footer.php';