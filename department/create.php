<?php
session_start();

if (empty($_POST['name']) || empty($_POST['phone'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to add new department. Required fields must be specified'
    ];
    header('Location: /department/new.php');
    die();
}

require_once '../classes/Department.php';

$department = new Department($_POST['name'], $_POST['phone']);
$department->create();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'New department has been added successfully'
];
header('Location: /index.php');