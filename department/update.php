<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

if (empty($_POST['name']) || empty($_POST['phone'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to update department information. Required fields must be specified'
    ];
    header('Location: /department/edit.php?id=' . $_POST['id']);
    die();
}

require_once '../classes/Department.php';

$department = new Department($_POST['name'], $_POST['phone']);
$department->setId($_POST['id']);
$department->update();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Department information has been updated successfully'
];
header('Location: /department/edit.php?id=' . $_POST['id']);