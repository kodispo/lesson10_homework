<?php
ini_set('display_errors', 'On');

session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once 'classes/HtmlMessageWriter.php';

require_once 'classes/Department.php';
require_once 'classes/HtmlDepartmentWriter.php';
require_once 'classes/Subject.php';
require_once 'classes/HtmlSubjectWriter.php';
require_once 'classes/Professor.php';
require_once 'classes/HtmlProfessorWriter.php';


/*
 * html output
 */
require_once 'parts/header.php';
echo HtmlMessageWriter::writeMessage($message);
require_once 'parts/dbControl.php';

$professors = new Professor();
if ($professors->hasTable()) {
    echo HtmlProfessorWriter::writeTable($professors->getAll());
}

$departments = new Department();
if ($departments->hasTable()) {
    echo HtmlDepartmentWriter::writeTable($departments->getAll());
}

$subjects = new Subject();
if ($subjects->hasTable()) {
    echo HtmlSubjectWriter::writeTable($subjects->getAll());
}

require_once 'parts/footer.php';