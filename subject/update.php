<?php
session_start();

if (empty($_POST['id'])) {
    header('Location: /index.php');
    die();
}

if (empty($_POST['name'])) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Failed to update subject information. Required fields must be specified'
    ];
    header('Location: /subject/edit.php?id=' . $_POST['id']);
    die();
}

require_once '../classes/Subject.php';

$subject = new Subject($_POST['name']);
$subject->setId($_POST['id']);
$subject->update();

$_SESSION['message'] = [
    'type' => 'success',
    'text' => 'Subject information has been updated successfully'
];
header('Location: /subject/edit.php?id=' . $_POST['id']);