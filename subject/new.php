<?php
session_start();
$message = '';
if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    unset($_SESSION['message']);
}
require_once '../classes/HtmlMessageWriter.php';

require_once '../classes/HtmlSubjectWriter.php';


/*
 * html output
 */
require_once '../parts/header.php';
echo HtmlMessageWriter::writeMessage($message);
echo HtmlSubjectWriter::writeCreateForm();
require_once '../parts/footer.php';